package application;

import java.util.ArrayList;
import java.util.List;

public class CadastroPessoas {

    private List<Pessoa> pessoas;

    public List<Pessoa> getPessoas() {
        return this.pessoas;
    }

    public CadastroPessoas() {
        this.pessoas = new ArrayList<Pessoa>();
    }

    public void adicionar(Pessoa pessoa) {
        if (pessoa.getNome() == null) {
            throw new PessoaSemNomeException();
        }
        this.pessoas.add(pessoa);
    }

    public void remover(Pessoa pessoa) {
        if (this.pessoas.isEmpty()) {
            throw new CadastrarVazioException();
        }
        this.pessoas.remove(pessoa);
    }
}
