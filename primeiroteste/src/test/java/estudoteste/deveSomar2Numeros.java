package estudoteste;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class deveSomar2Numeros {

    Calculadora calculadora;
    int numero1 = 10;
    int numero2 = 5;

    @Before
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void deveSomar2Numeros() {
        int resultado = calculadora.somar(numero1, numero2);
        Assertions.assertThat(resultado).isEqualTo(15);
    }

    @Test
    public void naoDeveSomarNumeroNegativo() {
        int resultado = calculadora.somar(numero1, numero2);
        Assertions.assertThat(resultado).isEqualTo(15);
    }

    @Test
    public void subtrair2Numeros() {
        int resultado = calculadora.subtrair(numero1, numero2);
        Assertions.assertThat(resultado).isEqualTo(5);
    }

    @Test
    public void multiplicar2Numeros() {
        int resultado = calculadora.multiplicar(numero1, numero2);
        Assertions.assertThat(resultado).isEqualTo(50);
    }

    @Test
    public void dividir2Numeros() {
        float resultado = calculadora.dividir(numero1, numero2);
        Assertions.assertThat(resultado).isEqualTo(2);
    }

    @Test(expected = ArithmeticException.class)
    public void naoPodeDividirPorZero() {
        //cenario
        int numero1 = 10, numero2 = 0;
        calculadora.dividir(numero1, numero2);
    }

}

class Calculadora {

    int somar(int num1, int num2) {

        if (num1 < 0 || num2 < 0) {
            throw new RuntimeException("Não pode somar números negativos!");
        }
        return num1 + num2;
    }

    int subtrair(int num1, int num2) {
        return num1 - num2;
    }

    int multiplicar(int num1, int num2) {
        return num1 * num2;
    }

    float dividir(int num1, int num2) {
        return num1 / num2;
    }

}
